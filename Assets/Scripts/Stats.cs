﻿namespace RPG2D
{
    [System.Serializable]
    public class Stats
    {
        public int hp;
        public int mp;
        public int attack;
        public int defense;
        public int magic;
        public int speed;
        public int luck;

        public int HP => hp;
        public int MP => mp;
        public int Attack => attack;
        public int Defense => defense;
        public int Magic => magic;
        public int Speed => speed;
        public int Luck => luck;

        public Stats() : this(0, 0, 0, 0, 0, 0, 0) { }

        public Stats(Stats other) : this(other.HP, other.MP, other.Attack, other.Defense, other.Magic, other.Speed, other.Luck) { }

        public Stats(int hp, int mp, int attack, int defense, int magic, int speed, int luck)
        {
            this.hp = hp;
            this.mp = mp;
            this.attack = attack;
            this.defense = defense;
            this.magic = magic;
            this.speed = speed;
            this.luck = luck;
        }

        public static Stats operator -(Stats a) => new Stats(
            -a.HP,
            -a.MP,
            -a.Attack,
            -a.Defense,
            -a.Magic,
            -a.Speed,
            -a.Luck);

        public static Stats operator +(Stats a, Stats b) => new Stats(
                a.HP + b.HP,
                a.MP + b.MP,
                a.Attack + b.Attack,
                a.Defense + b.Defense,
                a.Magic + b.Magic,
                a.Speed + b.Speed,
                a.Luck + b.Luck);

        public static Stats operator -(Stats a, Stats b) => a + (-b);
    }
}