﻿using System;

namespace RPG2D.UI
{
    public static class UIEvent
    {
        // character specific events
        public static event Action OnInfoChanged;
        public static event Action OnStatsChanged;
        public static event Action OnAuxiliaryStatsChanged;
        public static event Action OnEquipChanged;

        // item events
        public static event Action OnItemUsedEvent;

        public static void FireInfoChangedEvent()
        {
            OnInfoChanged?.Invoke();
        }

        public static void FireStatsChangedEvent()
        {
            OnStatsChanged?.Invoke();
        }

        public static void FireAuxiliaryStatsChangedEvent()
        {
            OnAuxiliaryStatsChanged?.Invoke();
        }

        public static void FireEquipChangedEvent()
        {
            OnEquipChanged?.Invoke();
        }

        public static void FireItemUsedEvent()
        {
            OnItemUsedEvent?.Invoke();
        }
    }
}