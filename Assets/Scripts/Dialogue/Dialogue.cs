﻿using UnityEngine;

namespace RPG2D.NPC
{
    [System.Serializable]
    public class Dialogue
    {
        public string name;

        [TextArea(3, 10)]
        public string[] dialogues;
    }
}