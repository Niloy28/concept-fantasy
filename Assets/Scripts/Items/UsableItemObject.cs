﻿using UnityEngine;

namespace RPG2D.Item
{
    public abstract class UsableItemObject : ItemObject
    {
        [SerializeField] protected bool isSingleTargetUse;

        public bool IsSingleTargetUse { get => isSingleTargetUse; }

        public abstract void Use(Character target = null);
    }
}