﻿using UnityEngine;

namespace RPG2D.Item
{
    [CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory System/Items/Equipment Object")]
    public class EquipmentObject : ItemObject
    {
        [SerializeField] protected Stats equipmentStats;

        public Stats EquipmentStats => equipmentStats;
    }
}