﻿using RPG2D.Inventory;
using RPG2D.UI;
using UnityEngine;

namespace RPG2D.Item
{
    [CreateAssetMenu(fileName = "New HP Healing Item", menuName = "Inventory System/Items/HP Healing Item")]
    public class HPHealingItemObject : RestorativeItemObject
    {
        public override void Use(Character target = null)
        {
            if (IsSingleTargetUse)
            {
                if (target.CurrentHP != 0)
                {
                    target.CurrentHP += healAmount;
                }
            }
            else
            {
                foreach (Character character in FindObjectsOfType<Character>())
                {
                    if (character.CurrentHP != 0)
                    {
                        character.CurrentHP += healAmount;
                    }
                }
            }

            PlayerInventoryManager.Instance.Inventories[ItemType.UsableItem].RemoveItem(this, 1);
            UIEvent.FireItemUsedEvent();
        }
    }
}