﻿using RPG2D.Item;
using System.Collections.Generic;
using UnityEngine;

namespace RPG2D.Inventory
{
    [CreateAssetMenu(fileName = "New Item Database", menuName = "Inventory System/Items/Item Database")]
    public class ItemDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private ItemObject[] items;

        public Dictionary<ItemObject, int> GetID { get; private set; }
        public Dictionary<int, ItemObject> GetItem { get; private set; }

        public void OnAfterDeserialize()
        {
            GetID = new Dictionary<ItemObject, int>();
            GetItem = new Dictionary<int, ItemObject>();

            for (int i = 0; i < items.Length; i++)
            {
                GetID[items[i]] = i;
                GetItem[i] = items[i];
            }
        }

        public void OnBeforeSerialize()
        {
        }
    }
}