﻿using UnityEngine;

namespace RPG2D.Item
{
    public abstract class HealingItemObject : ItemObject
    {
        [SerializeField] protected int healAmount;
        [SerializeField] protected bool groupHeal;
    }
}