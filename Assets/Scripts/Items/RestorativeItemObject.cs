﻿using UnityEngine;

namespace RPG2D.Item
{
    public abstract class RestorativeItemObject : UsableItemObject
    {
        [SerializeField] protected int healAmount;
    }
}