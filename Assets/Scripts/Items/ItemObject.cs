﻿using System;
using UnityEngine;

namespace RPG2D.Item
{
    public abstract class ItemObject : ScriptableObject, IComparable<ItemObject>
    {
        [SerializeField] protected string itemName;
        [SerializeField] protected int buyingPrice;

        [TextArea(3, 10)]
        [SerializeField] protected string description;

        public string ItemName => itemName;
        public int BuyingPrice => buyingPrice;
        public int SellingPrice => buyingPrice / 3;
        public string Description => description;

        public int CompareTo(ItemObject obj)
        {
            return itemName.CompareTo(obj.ItemName);
        }

        private void OnEnable()
        {
            itemName = name;
        }
    }
}