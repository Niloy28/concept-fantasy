﻿using RPG2D.Inventory;
using RPG2D.UI;
using UnityEngine;

namespace RPG2D.Item
{
    [CreateAssetMenu(fileName = "New MP Healing Item", menuName = "Inventory System/Items/MP Healing Item")]
    public class MPHealingItemObject : RestorativeItemObject
    {
        public override void Use(Character target = null)
        {
            if (IsSingleTargetUse)
            {
                target.CurrentMP += healAmount;
            }
            else
            {
                foreach (Character character in FindObjectsOfType<Character>())
                {
                    character.CurrentMP += healAmount;
                }
            }

            PlayerInventoryManager.Instance.Inventories[ItemType.UsableItem].RemoveItem(this, 1);
            UIEvent.FireItemUsedEvent();
        }
    }
}