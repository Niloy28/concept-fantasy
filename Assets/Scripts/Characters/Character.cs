﻿using RPG2D;
using RPG2D.Item;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Sprite characterPortrait;
    [SerializeField] private JobType job;

    private int currentHP;
    private int currentMP;

    public int CurrentHP {
        get => currentHP;
        set {
            currentHP = value;
            currentHP = Mathf.Clamp(currentHP, 0, Stats.HP);
        }
    }

    public int CurrentMP {
        get => currentMP;
        set {
            currentMP = value;
            currentMP = Mathf.Clamp(currentMP, 0, Stats.MP);
        }
    }

    public string CharacterName => gameObject.name.Split()[0];
    public Sprite CharacterPortrait => characterPortrait;
    public JobType Job => job;
    public int Level { get; set; }
    public Stats Stats { get; set; } = new Stats();
    public int EXP { get; set; }
    public int EXPForNextLevel { get; set; }
    public SerializableDictionary<CharacterEquipmentType, EquipmentObject> Equips { get; set; } = new SerializableDictionary<CharacterEquipmentType, EquipmentObject>();

    public void UpdateCharacterStats(EquipmentObject equipment, bool increaseStat = true)
    {
        if (increaseStat)
        {
            Stats += equipment.EquipmentStats;
        }
        else
        {
            Stats -= equipment.EquipmentStats;
        }
    }
}