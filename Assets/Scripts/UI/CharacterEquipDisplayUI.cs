﻿using System;
using TMPro;
using UnityEngine;

namespace RPG2D.UI
{
    public class CharacterEquipDisplayUI : MonoBehaviour
    {
        [SerializeField] private SerializableDictionary<CharacterEquipmentType, TMP_Text> equipFields;

        private void DisplayEquip()
        {
            var currentCharacterEquips = UIManager.Instance.CurrentCharacter.Equips;

            foreach (var characterEquipmentType in Enum.GetValues(typeof(CharacterEquipmentType)) as CharacterEquipmentType[])
            {
                if (!currentCharacterEquips.ContainsKey(characterEquipmentType) || currentCharacterEquips[characterEquipmentType] == null)
                {
                    equipFields[characterEquipmentType].text = "Empty";
                }
                else
                {
                    equipFields[characterEquipmentType].text = currentCharacterEquips[characterEquipmentType].ItemName;
                }
            }
        }

        private void OnEnable()
        {
            UIEvent.OnEquipChanged += DisplayEquip;
        }

        private void OnDisable()
        {
            UIEvent.OnEquipChanged -= DisplayEquip;
        }
    }
}