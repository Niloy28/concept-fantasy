﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RPG2D.UI
{
    [RequireComponent(typeof(ScrollRect))]
    public class ButtonSelectionScroller : MonoBehaviour
    {
        [SerializeField]
        private float lerpTime;

        private ScrollRect scrollRect;
        private List<Button> buttons;
        private int rowCount;
        private float verticalPosition;

        public void OnEnable()
        {
            SetupScroller();
        }

        public void Update()
        {
            ScrollToCurrentSelectedButton();
        }

        private void SetupScroller()
        {
            scrollRect = GetComponent<ScrollRect>();
            buttons = new List<Button>(GetComponentsInChildren<Button>());
            rowCount = Mathf.CeilToInt(buttons.Count / 2f);
        }

        private void ScrollToCurrentSelectedButton()
        {
            Button currentlySelectedButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();

            if (currentlySelectedButton != null)
            {
                int index = buttons.IndexOf(currentlySelectedButton);
                int currentRow = Mathf.FloorToInt(index / 2);

                verticalPosition = 1f - ((float)currentRow / (rowCount - 1));

                scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition, verticalPosition, Time.deltaTime / lerpTime);
            }
        }
    }
}