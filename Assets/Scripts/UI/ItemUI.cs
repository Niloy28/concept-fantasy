﻿using RPG2D.Inventory;
using RPG2D.Item;
using System.Collections.Generic;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class ItemUI : InventoryUI
    {
        public void SetupItemUI()
        {
            DestroyOldButtons();
            inventoryObject = PlayerInventoryManager.Instance.Inventories[ItemType.UsableItem];

            DisplayInventory(out List<Button> buttons, out List<ItemObject> items);

            FirstSelectedButton firstSelectedButton = MenuManager.Instance.Menus[MenuType.ItemMenu].GetComponent<FirstSelectedButton>();
            if (firstSelectedButton != null)
            {
                firstSelectedButton.Button = buttons[0];
            }

            for (int i = 0; i < items.Count; i++)
            {
                UsableItemObject item = items[i] as UsableItemObject;
                Button button = buttons[i];

                if (item.IsSingleTargetUse)
                {
                    button.onClick.AddListener(() => MenuManager.Instance.OpenCharacterSelectorForItemUse(item));
                }
                else
                {
                    button.onClick.AddListener(() => item.Use());
                }
            }
        }

        private void OnEnable()
        {
            UIEvent.OnItemUsedEvent += SetupItemUI;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            UIEvent.OnItemUsedEvent -= SetupItemUI;
        }
    }
}