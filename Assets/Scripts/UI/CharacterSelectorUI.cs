﻿using RPG2D.Item;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class CharacterSelectorUI : MonoBehaviour
    {
        [SerializeField] private Transform playerParty;
        [SerializeField] private Transform buttonPanel;
        [SerializeField] private Button buttonPrefab;

        private Character[] characters;
        private List<Button> instantiatedButtons;

        private Action<Character> menuToOpen;

        private void UpdatePartyCharacterList()
        {
            characters = playerParty.GetComponentsInChildren<Character>();
        }

        private void InstantiateAllButtons()
        {
            instantiatedButtons = new List<Button>();

            for (int i = 0; i != playerParty.childCount; i++)
            {
                Button button = Instantiate(buttonPrefab);
                button.transform.SetParent(buttonPanel, false);

                instantiatedButtons.Add(button);
            }
            GetComponent<FirstSelectedButton>().Button = instantiatedButtons[0];
        }

        private void AssignMenuToOpen(string characterMenu)
        {
            switch (characterMenu)
            {
                case "status":
                    menuToOpen = MenuManager.Instance.OpenStatusMenu;
                    break;

                case "equip":
                    menuToOpen = MenuManager.Instance.OpenEquipBaseMenu;
                    break;

                case "magic":
                    menuToOpen = MenuManager.Instance.OpenMagicMenu;
                    break;
            }
        }

        public void SetupButtonForMenu(string characterMenu)
        {
            DestroyOldButtons();
            InstantiateAllButtons();
            AssignMenuToOpen(characterMenu);

            for (int i = 0; i < instantiatedButtons.Count; i++)
            {
                UpdatePartyCharacterList();

                Character character = characters[i];

                instantiatedButtons[i].GetComponentInChildren<TMP_Text>().text = character.CharacterName;
                instantiatedButtons[i].onClick.AddListener(() => menuToOpen(character));
            }
        }

        public void SetupButtonForItemUse(UsableItemObject usable)
        {
            DestroyOldButtons();
            InstantiateAllButtons();

            for (int i = 0; i < instantiatedButtons.Count; i++)
            {
                UpdatePartyCharacterList();

                Character character = characters[i];

                instantiatedButtons[i].GetComponentInChildren<TMP_Text>().text = character.CharacterName;
                instantiatedButtons[i].onClick.AddListener(() => usable.Use(character));
            }
        }

        private void DestroyOldButtons()
        {
            if (instantiatedButtons != null)
            {
                foreach (var button in instantiatedButtons)
                {
                    Destroy(button.gameObject);
                }
            }
        }
    }
}