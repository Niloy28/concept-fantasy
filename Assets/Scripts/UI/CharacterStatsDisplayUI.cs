﻿using TMPro;
using UnityEngine;

namespace RPG2D.UI
{
    public class CharacterStatsDisplayUI : MonoBehaviour
    {
        public TMP_Text hp;
        public TMP_Text mp;
        public TMP_Text attack;
        public TMP_Text defense;
        public TMP_Text magic;
        public TMP_Text speed;
        public TMP_Text luck;

        private void DisplayStats()
        {
            Character currentCharacter = UIManager.Instance.CurrentCharacter;

            hp.text = currentCharacter.Stats.HP.ToString();
            mp.text = currentCharacter.Stats.MP.ToString();
            attack.text = currentCharacter.Stats.Attack.ToString();
            defense.text = currentCharacter.Stats.Defense.ToString();
            magic.text = currentCharacter.Stats.Magic.ToString();
            speed.text = currentCharacter.Stats.Speed.ToString();
            luck.text = currentCharacter.Stats.Luck.ToString();
        }

        private void OnEnable()
        {
            UIEvent.OnStatsChanged += DisplayStats;
        }

        private void OnDisable()
        {
            UIEvent.OnStatsChanged -= DisplayStats;
        }
    }
}