﻿using RPG2D.Item;
using UnityEngine;

namespace RPG2D.UI
{
    public class ItemHolder : MonoBehaviour
    {
        public ItemObject ItemObject { get; set; }
    }
}