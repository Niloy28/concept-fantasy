﻿using RPG2D.Inventory;
using RPG2D.Item;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class EquipmentUI : InventoryUI
    {
        public void SetupEquipUI(CharacterEquipmentType characterEquipmentType)
        {
            inventoryObject = PlayerInventoryManager.Instance.MapCharacterEquipmentTypeToInventory(characterEquipmentType);

            // add an unequipping button
            AddUnequipButton(characterEquipmentType);

            DisplayInventory(out List<Button> equipButtons, out List<ItemObject> items);

            // convert items into equipments
            List<EquipmentObject> equipments = items.ConvertAll(item => item as EquipmentObject);

            foreach (Button equipButton in equipButtons)
            {
                int index = equipButtons.IndexOf(equipButton);
                equipButton.onClick.AddListener(() => Equip(characterEquipmentType, equipments[index]));
            }
        }

        private void AddUnequipButton(CharacterEquipmentType characterEquipmentType)
        {
            Button unequipButton = Instantiate(buttonPrefab, contentField).GetComponent<Button>();

            unequipButton.GetComponentInChildren<TMP_Text>().text = "Unequip";
            unequipButton.onClick.AddListener(() => UnEquip(characterEquipmentType));

            var buttonRef = MenuManager.Instance.Menus[MenuType.EquipmentSelectionMenu].GetComponent<FirstSelectedButton>();
            if (buttonRef != null)
            {
                buttonRef.Button = unequipButton;
            }
        }

        private void Equip(CharacterEquipmentType characterEquipmentType, EquipmentObject equipment)
        {
            Character currentCharacter = UIManager.Instance.CurrentCharacter;
            var currentCharacterEquips = currentCharacter.Equips;

            if (!currentCharacterEquips.ContainsKey(characterEquipmentType) || currentCharacterEquips[characterEquipmentType] != equipment)
            {
                if (currentCharacterEquips.ContainsKey(characterEquipmentType))
                {
                    EquipmentObject oldEquip = currentCharacterEquips[characterEquipmentType];

                    currentCharacterEquips[characterEquipmentType] = null;
                    if (oldEquip != null)
                    {
                        currentCharacter.UpdateCharacterStats(oldEquip, increaseStat: false);
                        inventoryObject.AddItem(oldEquip, 1);
                    }
                }
                currentCharacterEquips[characterEquipmentType] = equipment;

                inventoryObject.RemoveItem(equipment, 1);
                currentCharacter.UpdateCharacterStats(equipment);
                UIEvent.FireStatsChangedEvent();
                UIEvent.FireEquipChangedEvent();
                MenuManager.Instance.GoBackToPrevMenu();
            }
        }

        private void UnEquip(CharacterEquipmentType characterEquipmentType)
        {
            Character currentCharacter = UIManager.Instance.CurrentCharacter;
            var currentCharacterEquips = currentCharacter.Equips;

            if (currentCharacterEquips.ContainsKey(characterEquipmentType) && currentCharacterEquips[characterEquipmentType] != null)
            {
                EquipmentObject oldEquip = currentCharacterEquips[characterEquipmentType];

                currentCharacterEquips[characterEquipmentType] = null;
                currentCharacter.UpdateCharacterStats(oldEquip, increaseStat: false);

                inventoryObject.AddItem(oldEquip, 1);
                UIEvent.FireStatsChangedEvent();
                UIEvent.FireEquipChangedEvent();
                MenuManager.Instance.GoBackToPrevMenu();
            }
        }
    }
}