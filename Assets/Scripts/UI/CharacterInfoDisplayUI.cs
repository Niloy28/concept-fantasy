﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class CharacterInfoDisplayUI : MonoBehaviour
    {
        public TMP_Text characterName;
        public Image characterPortrait;

        private void DisplayInfo()
        {
            Character currentCharacter = UIManager.Instance.CurrentCharacter;

            characterName.text = currentCharacter.CharacterName;
            characterPortrait.sprite = currentCharacter.CharacterPortrait;
        }

        private void OnEnable()
        {
            UIEvent.OnInfoChanged += DisplayInfo;
        }

        private void OnDisable()
        {
            UIEvent.OnInfoChanged -= DisplayInfo;
        }
    }
}