﻿using RPG2D.Item;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG2D.UI
{
    public class StatsChangeDisplayUI : MonoBehaviour
    {
        public TMP_Text hpChange;
        public TMP_Text mpChange;
        public TMP_Text attackChange;
        public TMP_Text defenseChange;
        public TMP_Text magicChange;
        public TMP_Text speedChange;
        public TMP_Text luckChange;

        private void Update()
        {
            DisplayStatsChange();
        }

        private void DisplayStatsChange()
        {
            Stats currentEquipmentStats = new Stats();
            Stats newEquipmentStats = new Stats();

            if (UIManager.Instance.CurrentCharacter.Equips.ContainsKey(UIManager.Instance.CurrentCharacterEquipmentType))
            {
                var currentCharacterEquipment = UIManager.Instance.CurrentCharacter.Equips[UIManager.Instance.CurrentCharacterEquipmentType];
                if (currentCharacterEquipment != null)
                {
                    currentEquipmentStats = currentCharacterEquipment.EquipmentStats;
                }
            }

            ItemHolder itemHolder = EventSystem.current.currentSelectedGameObject.GetComponent<ItemHolder>();
            if (itemHolder != null && itemHolder.ItemObject != null)
            {
                EquipmentObject equipment = itemHolder.ItemObject as EquipmentObject;
                newEquipmentStats = equipment.EquipmentStats;
            }

            Stats statsChange = newEquipmentStats - currentEquipmentStats;

            FormatStatChangeText(statsChange.HP, hpChange);
            FormatStatChangeText(statsChange.MP, mpChange);
            FormatStatChangeText(statsChange.Attack, attackChange);
            FormatStatChangeText(statsChange.Defense, defenseChange);
            FormatStatChangeText(statsChange.Magic, magicChange);
            FormatStatChangeText(statsChange.Speed, speedChange);
            FormatStatChangeText(statsChange.Luck, luckChange);
        }

        private void FormatStatChangeText(int statChange, TMP_Text text)
        {
            if (statChange > 0)
            {
                text.text = "+" + Mathf.Abs(statChange).ToString();
                text.color = Color.green;
            }
            else if (statChange < 0)
            {
                text.text = "-" + Mathf.Abs(statChange).ToString();
                text.color = Color.red;
            }
            else
            {
                text.text = Mathf.Abs(statChange).ToString();
                text.color = Color.gray;
            }
        }

        private void OnDisable()
        {
            ResetAllTexts();
        }

        private void ResetAllTexts()
        {
            hpChange.text = "";
            mpChange.text = "";
            attackChange.text = "";
            defenseChange.text = "";
            magicChange.text = "";
            speedChange.text = "";
            luckChange.text = "";
        }
    }
}