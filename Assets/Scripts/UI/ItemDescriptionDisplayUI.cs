﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace RPG2D.UI
{
    public class ItemDescriptionDisplayUI : MonoBehaviour
    {
        [SerializeField] private TMP_Text descriptionText;

        private void Update()
        {
            GameObject currentSelectedGO = EventSystem.current.currentSelectedGameObject;

            ItemHolder itemHolder = currentSelectedGO.GetComponent<ItemHolder>();
            if (itemHolder != null && itemHolder.ItemObject != null)
            {
                descriptionText.text = itemHolder.ItemObject.Description;
            }
            else
            {
                descriptionText.text = "";
            }
        }
    }
}