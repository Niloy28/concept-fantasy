﻿using UnityEngine;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class FirstSelectedButton : MonoBehaviour
    {
        [SerializeField] private Button button;

        public Button Button {
            get => button;
            set => button = value;
        }
    }
}