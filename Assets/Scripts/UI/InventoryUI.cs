﻿using RPG2D.Inventory;
using RPG2D.Item;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public abstract class InventoryUI : MonoBehaviour
    {
        [SerializeField] protected GameObject buttonPrefab;
        [SerializeField] protected RectTransform contentField;

        protected InventoryObject inventoryObject;

        protected void DisplayInventory(out List<Button> buttons, out List<ItemObject> items)
        {
            buttons = new List<Button>();
            items = new List<ItemObject>();

            List<InventorySlot> inventorySlots = new List<InventorySlot>(inventoryObject.Container);

            foreach (var inventorySlot in inventorySlots)
            {
                ItemObject item = inventorySlot.Item;
                int amount = inventorySlot.Amount;

                if (amount != 0)
                {
                    GameObject newButton = Instantiate(buttonPrefab, contentField);
                    newButton.GetComponentInChildren<TMP_Text>().text = item.ItemName + "   " + amount;
                    newButton.GetComponent<ItemHolder>().ItemObject = item;

                    buttons.Add(newButton.GetComponent<Button>());
                    items.Add(item);
                }
            }

            GridLayoutGroup gridLayoutGroup = GetComponentInChildren<GridLayoutGroup>();
            contentField.sizeDelta = new Vector2(0, ((inventorySlots.Count / 2) + 1) * gridLayoutGroup.cellSize.y);
        }

        protected void DestroyOldButtons()
        {
            foreach (Transform child in contentField)
            {
                Destroy(child.gameObject);
            }
        }

        protected virtual void OnDisable()
        {
            DestroyOldButtons();
        }
    }
}