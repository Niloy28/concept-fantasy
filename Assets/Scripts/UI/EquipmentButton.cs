﻿using UnityEngine;

namespace RPG2D.UI
{
    public class EquipmentButton : MonoBehaviour
    {
        [SerializeField] private CharacterEquipmentType characterEquipmentType;

        public void SetupEquipmentButton()
        {
            UIManager.Instance.CurrentCharacterEquipmentType = characterEquipmentType;
            MenuManager.Instance.OpenEquipmentSelectionMenu(characterEquipmentType);
        }
    }
}