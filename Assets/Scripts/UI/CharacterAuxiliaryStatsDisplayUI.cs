﻿using TMPro;
using UnityEngine;

namespace RPG2D.UI
{
    public class CharacterAuxiliaryStatsDisplayUI : MonoBehaviour
    {
        public TMP_Text characterJob;
        public TMP_Text level;
        public TMP_Text currentHP;
        public TMP_Text currentMP;
        public TMP_Text exp;
        public TMP_Text nextLevelExp;

        private void DisplayAuxiliaryStats()
        {
            Character currentCharacter = UIManager.Instance.CurrentCharacter;

            characterJob.text = currentCharacter.Job.ToString();
            level.text = currentCharacter.Level.ToString();
            currentHP.text = currentCharacter.CurrentHP.ToString();
            currentMP.text = currentCharacter.CurrentMP.ToString();
            exp.text = currentCharacter.EXP.ToString();
            nextLevelExp.text = currentCharacter.EXPForNextLevel.ToString();
        }

        private void OnEnable()
        {
            UIEvent.OnAuxiliaryStatsChanged += DisplayAuxiliaryStats;
        }

        private void OnDisable()
        {
            UIEvent.OnAuxiliaryStatsChanged -= DisplayAuxiliaryStats;
        }
    }
}