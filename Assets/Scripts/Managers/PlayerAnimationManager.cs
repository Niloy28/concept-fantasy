﻿using RPG2D.Input;
using UnityEngine;

namespace RPG2D.Animation
{
    public class PlayerAnimationManager : MonoBehaviour
    {
        private GameObject playerParty;
        private Animator leadingMemberAnimator;

        public static PlayerAnimationManager Instance { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            playerParty = GameObject.FindGameObjectWithTag("Player");
            SetupLeadingPartyMember();
        }

        private void SetupLeadingPartyMember()
        {
            for (int i = 1; i < playerParty.transform.childCount; i++)
            {
                playerParty.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }

            leadingMemberAnimator = playerParty.transform.GetChild(0).gameObject.GetComponent<Animator>();
        }

        public void AnimatePlayer()
        {
            float xData = InputManager.Instance.MoveData.x;
            float yData = InputManager.Instance.MoveData.y;

            /*
             * The diagonal movements will play the up or down animation.
             */
            leadingMemberAnimator.SetBool("isMovingVertically", yData != 0);
            leadingMemberAnimator.SetBool("isMoving", xData != 0 || yData != 0);
            leadingMemberAnimator.SetFloat("xSpeed", xData);
            leadingMemberAnimator.SetFloat("ySpeed", yData);
        }
    }
}