﻿using UnityEngine;

namespace RPG2D.UI
{
    public class UIManager : MonoBehaviour
    {
        public Character CurrentCharacter { get; set; }
        public CharacterEquipmentType CurrentCharacterEquipmentType { get; set; }

        public static UIManager Instance { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}