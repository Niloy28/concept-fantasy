﻿using RPG2D.NPC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovementManager : MonoBehaviour
{
    private NPCMovement[] movements;
    
    public static NPCMovementManager Instance { get; private set; }

    private void ImplementSingleton()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Awake()
    {
        ImplementSingleton();
        movements = FindObjectsOfType<NPCMovement>();
    }

    public void DisableAllNPCMovement()
    {
        foreach (NPCMovement movement in movements)
        {
            movement.DisableMovement();
        }
    }
}
