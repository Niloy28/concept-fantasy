﻿using RPG2D.Item;
using UnityEngine;

namespace RPG2D.Inventory
{
    public class PlayerInventoryManager : MonoBehaviour
    {
        [SerializeField] private SerializableDictionary<ItemType, InventoryObject> inventories;

        public static PlayerInventoryManager Instance { get; private set; }

        public SerializableDictionary<ItemType, InventoryObject> Inventories => inventories;

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.S))
            {
                SaveInventory();
            }
            if (UnityEngine.Input.GetKeyDown(KeyCode.L))
            {
                LoadInventory();
            }
        }

        public void SaveInventory()
        {
            foreach (var inventory in Inventories.Values)
            {
                inventory.Save();
            }
        }

        public void LoadInventory()
        {
            foreach (var inventory in Inventories.Values)
            {
                inventory.Load();
            }
        }

        public InventoryObject MapCharacterEquipmentTypeToInventory(CharacterEquipmentType characterEquipmentType)
        {
            switch (characterEquipmentType)
            {
                case CharacterEquipmentType.Weapon:
                    return Inventories[ItemType.Weapon];

                case CharacterEquipmentType.Armor:
                    return Inventories[ItemType.Armor];

                case CharacterEquipmentType.Accessory1:
                case CharacterEquipmentType.Accessory2:
                    return Inventories[ItemType.Accessory];
            }

            return null;
        }
    }
}