﻿using RPG2D.UI;
using RPG2D.NPC;
using UnityEngine;

namespace RPG2D.Input
{
    public class InputManager : MonoBehaviour
    {
        private PlayerInputs playerInputs;

        public static InputManager Instance { get; private set; }

        public Vector2 MoveData { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
            SetupPlayerInputs();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void SetupPlayerInputs()
        {
            playerInputs = new PlayerInputs();

            // Player movement
            playerInputs.Player.Movement.performed += ctx => MoveData = ctx.ReadValue<Vector2>();
            playerInputs.Player.Movement.canceled += _ => MoveData = Vector2.zero;
            playerInputs.Player.Interact.performed += _ => NPCDialogueManager.Instance.DisplayNextSentence();

            // Menu handling
            playerInputs.UI.OpenInGameMenu.performed += _ => MenuManager.Instance.OpenInGameMenu();
            playerInputs.UI.OpenPauseMenu.performed += _ => MenuManager.Instance.OpenPauseMenu();
            playerInputs.UI.Cancel.performed += _ => MenuManager.Instance.GoBackToPrevMenu();
        }

        public void EnablePlayerMovement()
        {
            playerInputs.Player.Movement.Enable();
        }

        public void DisablePlayerMovement()
        {
            playerInputs.Player.Movement.Disable();
        }

        private void OnEnable()
        {
            playerInputs.Enable();
        }

        private void OnDisable()
        {
            playerInputs.Disable();
        }
    }
}