﻿using RPG2D.Input;
using RPG2D.Item;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RPG2D.UI
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] private SerializableDictionary<MenuType, Canvas> menus;

        private EventSystem eventSystem;
        private Stack<Canvas> menuStack;
        private Stack<Button> selectedButtons;

        public static MenuManager Instance { get; private set; }

        public SerializableDictionary<MenuType, Canvas> Menus => menus;

        private void Awake()
        {
            ImplementSingleton();
        }

        private void Start()
        {
            CacheReferences();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void CacheReferences()
        {
            eventSystem = EventSystem.current;
            menuStack = new Stack<Canvas>();
            selectedButtons = new Stack<Button>();
        }

        public void OpenMenu(MenuType menuType)
        {
            Canvas menu = menus[menuType];

            if (menuStack.Count != 0 && menuStack.Peek() != menu)
            {
                CanvasGroup canvasGroup = menuStack.Peek().gameObject.GetComponent<CanvasGroup>();

                if (canvasGroup != null)
                {
                    canvasGroup.interactable = false;
                }
            }

            // open if no menu already no menu open (for in-game and pause menu)
            // also do not open multiple instances of same menu
            if (menuStack.Count == 0 || menuStack.Peek() != menu)
            {
                menuStack.Push(menu);
                menu.gameObject.SetActive(true);
                InputManager.Instance.DisablePlayerMovement();
                NPCMovementManager.Instance.DisableAllNPCMovement();
            }

            var buttonRef = menu.GetComponent<FirstSelectedButton>();
            if (buttonRef != null)
            {
                Button button = buttonRef.Button;

                eventSystem.SetSelectedGameObject(button.gameObject);
                if (!selectedButtons.Contains(button))
                {
                    selectedButtons.Push(button);
                }
            }
        }

        public void OpenOptionsMenu()
        {
            Canvas optionMenu = menus[MenuType.OptionMenu];
            menuStack.Push(optionMenu);
            optionMenu.gameObject.SetActive(true);
        }

        public void OpenInGameMenu()
        {
            OpenMenu(MenuType.InGameMenu);
        }

        public void OpenCharacterSelectorForItemUse(UsableItemObject usable)
        {
            Canvas characterSelectorMenu = menus[MenuType.CharacterSelectorMenu];

            CharacterSelectorUI characterSelectorUI = characterSelectorMenu.GetComponent<CharacterSelectorUI>();

            characterSelectorUI.SetupButtonForItemUse(usable);
            OpenMenu(MenuType.CharacterSelectorMenu);
        }

        public void OpenCharacterSelectorForMenu(string menuToOpen)
        {
            Canvas characterSelectorMenu = menus[MenuType.CharacterSelectorMenu];

            CharacterSelectorUI characterSelectorUI = characterSelectorMenu.GetComponent<CharacterSelectorUI>();

            characterSelectorUI.SetupButtonForMenu(menuToOpen);
            OpenMenu(MenuType.CharacterSelectorMenu);
        }

        public void OpenEquipBaseMenu(Character character)
        {
            UIManager.Instance.CurrentCharacter = character;

            OpenMenu(MenuType.EquipBaseMenu);
            UIEvent.FireInfoChangedEvent();
            UIEvent.FireStatsChangedEvent();
            UIEvent.FireEquipChangedEvent();
        }

        public void OpenEquipmentSelectionMenu(CharacterEquipmentType characterEquipmentType)
        {
            EquipmentUI equipmentUI = Menus[MenuType.EquipmentSelectionMenu].GetComponentInChildren<EquipmentUI>();

            equipmentUI.SetupEquipUI(characterEquipmentType);
            OpenMenu(MenuType.EquipmentSelectionMenu);
        }

        public void OpenStatusMenu(Character character)
        {
            UIManager.Instance.CurrentCharacter = character;

            OpenMenu(MenuType.StatusMenu);
            UIEvent.FireInfoChangedEvent();
            UIEvent.FireStatsChangedEvent();
            UIEvent.FireAuxiliaryStatsChangedEvent();
            UIEvent.FireEquipChangedEvent();
        }

        public void OpenItemMenu()
        {
            ItemUI itemUI = Menus[MenuType.ItemMenu].GetComponentInChildren<ItemUI>();

            itemUI.SetupItemUI();
            OpenMenu(MenuType.ItemMenu);
        }

        public void OpenMagicMenu(Character character)
        {
        }

        public void OpenPauseMenu()
        {
            if (menuStack.Count == 0)
            {
                OpenMenu(MenuType.PauseMenu);
            }
        }

        public void GoBackToPrevMenu()
        {
            try
            {
                Canvas currMenu = menuStack.Pop();

                CanvasGroup oldMenuCanvasGroup = currMenu.GetComponent<CanvasGroup>();
                if (oldMenuCanvasGroup != null)
                {
                    oldMenuCanvasGroup.interactable = true;
                }

                currMenu.gameObject.SetActive(false);

                if (menuStack.Count == 0)
                {
                    selectedButtons.Clear();
                    InputManager.Instance.EnablePlayerMovement();
                }
                else
                {
                    menuStack.Peek().gameObject.SetActive(true);
                    CanvasGroup canvasGroup = menuStack.Peek().gameObject.GetComponent<CanvasGroup>();

                    if (canvasGroup != null)
                    {
                        canvasGroup.interactable = true;
                    }

                    try
                    {
                        while (menuStack.Count < selectedButtons.Count)
                        {
                            _ = selectedButtons.Pop();
                        }

                        eventSystem.SetSelectedGameObject(selectedButtons.Peek().gameObject);
                    }
                    catch (Exception)
                    {
                        Debug.Log("Button Stack Empty.");
                    }
                }
            }
            catch (Exception)
            {
                Debug.Log("Menu Stack Empty");
            }
        }
    }
}