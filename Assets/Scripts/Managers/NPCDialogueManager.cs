﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RPG2D.NPC
{
    public class NPCDialogueManager : MonoBehaviour
    {
        [SerializeField] private GameObject dialoguePanel;
        [SerializeField] private TMP_Text nameText;
        [SerializeField] private TMP_Text dialogueText;

        private Queue<string> sentences;

        public static NPCDialogueManager Instance { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        public void StartDialogue(Dialogue dialogue)
        {
            ShowDialoguePanel();
            nameText.text = dialogue.name;
            sentences = new Queue<string>(dialogue.dialogues);

            DisplayNextSentence();
        }

        public void EndDialogue()
        {
            HideDialoguePanel();
        }

        private void ShowDialoguePanel()
        {
            dialoguePanel.SetActive(true);
        }

        public void DisplayNextSentence()
        {
            if (sentences != null && sentences.Count != 0)
            {
                dialogueText.text = sentences.Dequeue();
            }
            else
            {
                EndDialogue();
            }
        }

        private void HideDialoguePanel()
        {
            dialoguePanel.SetActive(false);
        }
    }
}