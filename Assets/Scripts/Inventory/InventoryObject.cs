﻿using RPG2D.Item;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace RPG2D.Inventory
{
    [CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory System/Inventory/Inventory Object")]
    public class InventoryObject : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<InventorySlot> container;

        private ItemDatabaseObject database;

        public List<InventorySlot> Container => container;

        private void OnEnable()
        {
            container = new List<InventorySlot>();
            AssignItemDatabase();
        }

        private void AssignItemDatabase()
        {
            string databasePrefix = name.Split()[0];

            database = Resources.Load<ItemDatabaseObject>("Database/" + databasePrefix + " Database");
        }

        public void AddItem(ItemObject item, int amount)
        {
            foreach (InventorySlot slot in container)
            {
                if (slot.Item == item)
                {
                    slot.AddAmount(amount);
                    return;
                }
            }

            container.Add(new InventorySlot(database.GetID[item], item, amount));
        }

        public void RemoveItem(ItemObject item, int amount)
        {
            foreach (InventorySlot slot in container)
            {
                if (slot.item == item)
                {
                    slot.RemoveAmount(amount);
                }
            }
        }

        public void Save()
        {
            string inventoryData = JsonUtility.ToJson(this, prettyPrint: true);
            string savePath = string.Concat(Application.persistentDataPath, "/saves/inventory/");

            if (!Directory.Exists(savePath))
                Directory.CreateDirectory(savePath);

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = File.Create(Path.Combine(savePath, name + ".sav"));

            binaryFormatter.Serialize(fileStream, inventoryData);
            fileStream.Close();
        }

        public void Load()
        {
            string loadPath = string.Concat(Application.persistentDataPath, "/saves/inventory/", name, ".sav");

            if (File.Exists(loadPath))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream fileStream = File.OpenRead(loadPath);
                JsonUtility.FromJsonOverwrite(binaryFormatter.Deserialize(fileStream).ToString(), this);

                fileStream.Close();
            }
        }

        public void OnAfterDeserialize()
        {
            for (int i = 0; i < container.Count; i++)
            {
                container[i].item = database.GetItem[container[i].ID];
            }

            container.Sort();
        }

        public void OnBeforeSerialize()
        {
        }
    }
}