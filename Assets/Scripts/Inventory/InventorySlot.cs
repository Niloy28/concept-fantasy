﻿using RPG2D.Item;
using System;

namespace RPG2D.Inventory
{
    [System.Serializable]
    public class InventorySlot : IComparable<InventorySlot>
    {
        public int id;
        public ItemObject item;
        public int amount;

        public int ID => id;
        public ItemObject Item => item;
        public int Amount => amount;

        public InventorySlot(ItemObject item, int amount) : this(0, item, amount) { }

        public InventorySlot(int id, ItemObject item, int amount)
        {
            this.id = id;
            this.item = item;
            this.amount = amount;
        }

        public void AddAmount(int amountAdded)
        {
            amount += amountAdded;
        }

        public void RemoveAmount(int amountRemoved)
        {
            amount -= amountRemoved;
        }

        public int CompareTo(InventorySlot obj)
        {
            return item.CompareTo(obj.item);
        }
    }
}