﻿using System;

[Serializable]
public enum MenuType : int
{
    PauseMenu,
    OptionMenu,
    InGameMenu,
    CharacterSelectorMenu,
    ItemMenu,
    MagicMenu,
    EquipBaseMenu,
    EquipmentSelectionMenu,
    StatusMenu,
}