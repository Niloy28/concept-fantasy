﻿public enum CharacterEquipmentType
{
    Weapon,
    Armor,
    Accessory1,
    Accessory2
}