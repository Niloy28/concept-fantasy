﻿namespace RPG2D.Item
{
    public enum ItemType
    {
        Weapon, Armor, Accessory, UsableItem
    }
}