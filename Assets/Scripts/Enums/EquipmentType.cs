﻿namespace RPG2D.Item
{
    public enum EquipmentType
    {
        Weapon,
        Armor,
        Accessories
    }
}