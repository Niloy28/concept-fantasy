﻿namespace RPG2D
{
    public enum JobType : byte
    {
        Paladin = 0,
        WindKnight = 1,
        WhiteMage = 2,
        BlackMage = 3
    }
}