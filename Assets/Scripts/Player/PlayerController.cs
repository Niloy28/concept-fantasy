﻿using RPG2D.Animation;
using RPG2D.Input;
using UnityEngine;

namespace RPG2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour
    {
        [Range(1f, 10f)]
        [SerializeField] private float movementSpeed = 5f;

        private Rigidbody2D rb;

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            MovePlayer();
        }

        private void MovePlayer()
        {
            rb.velocity = InputManager.Instance.MoveData * movementSpeed;
            PlayerAnimationManager.Instance.AnimatePlayer();
        }
    }
}