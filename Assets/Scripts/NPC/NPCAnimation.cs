﻿using UnityEngine;

namespace RPG2D.NPC
{
    public class NPCAnimation : MonoBehaviour
    {
        private NPCMovement npcMovement;
        private Animator animator;

        private void Awake()
        {
            CacheReferences();
        }

        private void CacheReferences()
        {
            npcMovement = GetComponent<NPCMovement>();
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            animator.SetBool("movingUp", npcMovement.Velocity.y > 0);
            animator.SetBool("movingDown", npcMovement.Velocity.y < 0);
            animator.SetBool("movingRight", npcMovement.Velocity.x > 0);
            animator.SetBool("movingLeft", npcMovement.Velocity.x < 0);
            animator.SetBool("isMoving", npcMovement.Velocity.magnitude > 0);
        }
    }
}