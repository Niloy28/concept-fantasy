﻿using UnityEngine;

namespace RPG2D.NPC
{
    public class NPCDialogue : MonoBehaviour
    {
        [SerializeField] private Dialogue dialogue;

        private NPCMovement npcMovement;

        private void Awake()
        {
            npcMovement = GetComponentInParent<NPCMovement>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                NPCDialogueManager.Instance.StartDialogue(dialogue);
                npcMovement.DisableMovement();
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                NPCDialogueManager.Instance.EndDialogue();
                npcMovement.EnableMovement();
            }
        }
    }
}