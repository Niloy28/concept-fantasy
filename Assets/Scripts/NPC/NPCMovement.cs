﻿using System.Collections;
using UnityEngine;

namespace RPG2D.NPC
{
    public class NPCMovement : MonoBehaviour
    {
        [Range(0f, 5f)] [SerializeField] private float minMoveDistance = 0.8f;
        [Range(0f, 5f)] [SerializeField] private float maxMoveDistance = 1.2f;
        [Range(0f, 2f)] [SerializeField] private float minWaitBetweenMove = 1f;
        [Range(2.5f, 5f)] [SerializeField] private float maxWaitBetweenMove = 3f;
        [SerializeField] private float movementSpeed = 100f;

        private bool isMoving;
        private bool isMovementAllowed = true;
        private Rigidbody2D rb;

        public int Axis { get; set; }
        public int Direction { get; set; }

        public Vector2 Velocity { get; set; }

        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            if (!isMoving && isMovementAllowed)
            {
                StartCoroutine(MoveRandomly());
            }
        }

        private IEnumerator MoveRandomly()
        {
            isMoving = true;

            Axis = Random.Range(0, 1 + 1);
            Direction = Random.Range(-1, 1 + 1);
            float moveDistance = Random.Range(minMoveDistance, maxMoveDistance);

            float moveTimer = moveDistance / movementSpeed;
            Velocity = new Vector2((1 - Axis) * Direction * movementSpeed, Axis * Direction * movementSpeed);
            rb.velocity = Velocity;

            yield return new WaitForSecondsRealtime(moveTimer);

            Velocity = Vector2.zero;
            rb.velocity = Velocity;
            yield return new WaitForSecondsRealtime(Random.Range(minWaitBetweenMove, maxWaitBetweenMove));

            isMoving = false;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            rb.velocity *= -1;
        }

        public void EnableMovement()
        {
            isMovementAllowed = true;
        }

        public void DisableMovement()
        {
            rb.velocity = Vector2.zero;
            isMovementAllowed = false;
        }
    }
}